package hiker_test

import (
	"fizzbuzz/hiker"
	"testing"
)

func TestReturnFIZZWhenInputIsThree(t *testing.T) {
	result := hiker.FizzBuzz(3)

	if result != "FIZZ" {
		t.Errorf("'%+v' should be 'FIZZ'", result)
	}

}

func TestReturnBUZZWhenInputIsFive(t *testing.T) {
	result := hiker.FizzBuzz(5)

	if result != "BUZZ" {
		t.Errorf("'%+v' should be 'BUZZ'", result)
	}
}

func TestReturnDefaultNumberWhenInputIsNotThreeOrFive(t *testing.T) {
	result := hiker.FizzBuzz(2)

	if result != "2" {
		t.Errorf("'%+v' should be '2'", result)
	}
}

func TestReturnFIZZWhenInputIsSix(t *testing.T) {
	result := hiker.FizzBuzz(6)

	if result != "FIZZ" {
		t.Errorf("'%+v' should be 'FIZZ'", result)
	}
}

func TestReturnBUZZWhenInputIsTen(t *testing.T) {
	result := hiker.FizzBuzz(10)

	if result != "BUZZ" {
		t.Errorf("'%+v' should be 'BUZZ'", result)
	}
}

func TestReturnFIZZBUZZWhenInputIsFifteen(t *testing.T) {
	result := hiker.FizzBuzz(15)

	if result != "FIZZBUZZ" {
		t.Errorf("'%+v' should be 'FIZZBUZZ'", result)
	}
}

func TestReturnZeroWhenInputIsZero(t *testing.T) {
	result := hiker.FizzBuzz(0)

	if result != "0" {
		t.Errorf("'%+v' should be '0'", result)
	}
}
